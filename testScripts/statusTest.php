<?php

function output($str) {
    echo $str;
    ob_end_flush();
    ob_flush();
    flush();
    ob_start();
}

//for ($i=0; $i<20; $i++) {
//	echo "\rOutput $i";
//	sleep(2);
//}

ob_start();
for ($i=0 ; $i<=10 ; $i++) {
//  output("Progress: $i %   \r");
	echo "Progress: $i %   \r";
	sleep(1);
}

ob_end_clean();
echo "Done";

//echo "Progress :      ";  // 5 characters of padding at the end
//for ($i=0 ; $i<=10 ; $i++) {
//    echo "\e[5D";      // Move 5 characters backward
//    echo str_pad($i, 3, ' ', STR_PAD_LEFT) . " %";    // Output is always 5 characters long
//    sleep(1);           // wait for a while, so we see the animation
//}

?>