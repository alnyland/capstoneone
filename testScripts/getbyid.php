<?php

$runningCSV = false;

if (isset($_REQUEST['csv'])) {
	$runningCSV = true;
}

$rid = "479ZUTSD013";
if (isset($_REQUEST['id'])) {
	$rid = $_REQUEST['id'];
	if (!$runningCSV) {
		echo "Updated ID to $rid";
	}
}


// Retrieve and display results
$opt = array(
  'http' => array(
  	'method' => 'GET'
  )
);
$content = stream_context_create($opt);
//$output = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?CMD=Get&FORMAT_TYPE=Text&RID=$rid", false, $content);
$output = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=Text&DESCRIPTIONS=100&ALIGNMENTS=100&FORMAT_OBJECT=Alignment&QUERY_INDEX=0&DOWNLOAD_TEMPL=Results&CMD=Get");
// Blast.cgi?RESULTS_FILE=on&RID=43V0VVW8016&FORMAT_TYPE=CSV&DESCRIPTIONS=100&FORMAT_OBJECT=Alignment&ALIGNMENT_VIEW=Tabular&CMD=Get

// get CSV data
// hit table
$hittablecontent = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=CSV&DESCRIPTIONS=100&FORMAT_OBJECT=Alignment&ALIGNMENT_VIEW=Tabular&CMD=Get", false, $content);
$descriptionstable = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=CSV&DESCRIPTIONS=100&FORMAT_OBJECT=Alignment&QUERY_INDEX=0&DOWNLOAD_TEMPL=Results&CMD=Get");
//text_download_csv($hittablecontent, 'hittabletest.csv');

if ($runningCSV) {
	$fname = "output.csv";
	
	// mutate data to output
//	$newtext = $hittablecontent; // update this to be combined data
	$lines = explode(PHP_EOL, $hittablecontent);
	$array = array();
	foreach ($lines as $line) {
		$array[] = str_getcsv($line);
	}
//	print_r($hittablecontent);
	$otherlines = explode(PHP_EOL, $descriptionstable);
	$otherarray = array();
	foreach ($otherlines as $otherline) {
		$otherarray[] = str_getcsv($otherline);
	}
//	print_r($descriptionstable);
	
	// construct dictionary of Accession -> Scientific Name
	$namelookups = array();
	foreach ($otherarray as $desc) {
		$name = $desc[1];
		$accession = explode(",", $desc[10])[1];
		$accession = substr($accession, 1, strlen($accession)-3);
		$namelookups[$accession] = $name;
	}
	
	foreach($array as &$piece) {
		$acc = $piece[1];
		$piece[] = $namelookups[$acc];
	}
	
	// convert back to a string to output as CSV
	$newtext = "";
	foreach ($array as $part) {
		if ($part[0]) {
			$partstr = implode(',', $part);
			$partstr .= "\n";
			$newtext .= $partstr;
		}
	}
	
	// Add headers
	$headertext = ['query acc.ver', 'subject acc.ver', '% identity', 'alignment length', 'mismatches', 'gap opens', 'q. start', 'q. end', 's. start', 's. end', 'evalue', 'bit score', '% positives', 'Scientific Name'];
	$headertext = implode(",", $headertext);
	$newtext = "$headertext\n$newtext";
	
	header( 'Content-Type: application/csv' );
    header( 'Content-Disposition: attachment; filename="' . $fname . '";' );

    // clean output buffer
    ob_end_clean();
    
    $handle = fopen( 'php://output', 'w' );

    fwrite($handle, $newtext);

    fclose( $handle );

    // flush buffer
    ob_flush();
    
    // use exit to get rid of unexpected output afterward
    exit();
} else {

	file_put_contents("/tmp/$rid.txt", $output);
	echo "<h1>Hit Table</h1>";
	echo "<pre>$hittablecontent</pre>";
	echo "<h1>Description Table</h1>";
	echo "<pre>$descriptionstable</pre>";
	echo "<h1>Similarity file?</h1>";
	echo "<pre>$output</pre>";
	
}
?>
