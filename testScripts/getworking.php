<?php

$output = file_get_contents("5DTCMBKW013-Alignment.txt");
$output = htmlentities($output);
$similarities = true;

?>

<!doctype html>
<html>
	<head>
		<title>BIOL 317 Scripts S2022</title>
		<script type="text/javascript" src="script.js"></script>
		<script>
			var myContent = "";
			onload = function () {
				if (document.getElementById('similarities')) {
					myContent = document.getElementById('output').innerHTML.trim();
					Array.prototype.slice.call(document.getElementsByTagName('pre')).forEach((l) => {
						l.parentNode.removeChild(l);
					});
					let outputElem = document.createElement('pre');
					outputElem.setAttribute("id", "output");
					document.body.appendChild(outputElem);
					dealWithContent(myContent);
				}
			}
		</script>
		<style type="text/css">
			.sample-print {
				display: flex;
				justify-content: space-between;
			}
		</style>
	</head>
	<body id="<?php echo $similarities ? 'similarities' : ''; ?>">
		<?php if ($similarities) : ?>
			<?php echo "<pre id='output'>$output</pre>"; ?>
			<pre id="output"></pre>
		<?php else: ?>
		<h1>BIOL 317 Spring 2022</h1>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<label>RID:</label>
			<input type="text" name="rid">
			<br>
			<br>
			<input type="submit" name="similarities" value="Similarities"/>
			<input type="submit" name="combo" value="Combine CSV Output"/>
		</form>
		<?php endif; ?>
	</body>
</html>