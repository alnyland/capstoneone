RID: 5DTCMBKW013	
Job Title:sp|P0CS93.1|				
Program: BLASTP 
Query: RecName: Full=Galactose oxidase; Short=GAO; Short=GO; Short=GOase; Flags: Precursor [Fusarium graminearum] ID: P0CS93.1(amino acid) Length: 680
Database: nr All non-redundant GenBank CDS translations+PDB+SwissProt+PIR+PRF excluding environmental samples from WGS projects
	
Sequences producing significant alignments:
                                                                  Scientific      Common                     Max    Total Query   E   Per.   Acc.                        
Description                                                       Name            Name            Taxid      Score  Score cover Value Ident  Len        Accession        
PREDICTED: kelch-like protein 7 isoform X1 [Lipotes vexillifer]   Lipotes vexi... Yangtze Rive... 118797     45.8   45.8  16%   0.002 29.84  586        XP_007447506.1   
kelch-like protein 7 isoform X1 [Orcinus orca]                    Orcinus orca    killer whale    9733       45.8   45.8  16%   0.002 29.84  586        XP_004263486.1   
kelch-like protein 7 isoform X2 [Orcinus orca]                    Orcinus orca    killer whale    9733       45.8   45.8  16%   0.002 29.84  538        XP_004263487.1   
PREDICTED: kelch-like protein 7 isoform X4 [Lipotes vexillifer]   Lipotes vexi... Yangtze Rive... 118797     45.4   45.4  16%   0.002 29.84  510        XP_007447509.1   
kelch-like protein 7 [Balaenoptera acutorostrata scammoni]        Balaenoptera... NA              310752     45.1   45.1  16%   0.003 29.84  361        XP_007170379.1   
hypothetical protein E2I00_016144 [Balaenoptera physalus]         Balaenoptera... Fin whale       9770       44.3   44.3  16%   0.003 29.84  287        KAB0388793.1     
hypothetical protein E2I00_018042 [Balaenoptera physalus]         Balaenoptera... Fin whale       9770       42.4   42.4  12%   0.024 28.41  630        KAB0395624.1     
PREDICTED: kelch-like protein 36 isoform X3 [Lipotes vexillifer]  Lipotes vexi... Yangtze Rive... 118797     42.0   42.0  12%   0.025 31.31  553        XP_007468473.1   
kelch-like protein 36 isoform X3 [Physeter catodon]               Physeter cat... sperm whale     9755       42.0   42.0  12%   0.026 31.31  576        XP_028333635.1   
influenza virus NS1A-binding protein [Balaenoptera acutorostra... Balaenoptera... NA              310752     42.0   42.0  12%   0.026 28.41  642        XP_007167032.1   
PREDICTED: kelch-like protein 36 isoform X1 [Lipotes vexillifer]  Lipotes vexi... Yangtze Rive... 118797     42.0   42.0  12%   0.027 31.31  639        XP_007468471.1   
hypothetical protein E2I00_019412 [Balaenoptera physalus]         Balaenoptera... Fin whale       9770       42.0   42.0  12%   0.028 31.31  639        KAB0392230.1     
kelch-like protein 36 isoform X1 [Delphinapterus leucas]          Delphinapter... beluga whale    9749       42.0   42.0  12%   0.028 31.31  639        XP_022438730.1   
kelch-like protein 36 isoform X1 [Phocoena sinus]                 Phocoena sinus  vaquita         42100      42.0   42.0  12%   0.028 31.31  639        XP_032469049.1   
kelch-like protein 36 isoform X1 [Balaenoptera musculus]          Balaenoptera... Blue whale      9771       42.0   42.0  12%   0.029 31.31  639        XP_036689786.1   
kelch-like protein 36 isoform X1 [Globicephala melas]             Globicephala... long-finned ... 9731       42.0   42.0  12%   0.029 31.31  639        XP_030719029.1   
kelch-like protein 36 isoform X1 [Tursiops truncatus]             Tursiops tru... common bottl... 9739       42.0   42.0  12%   0.030 31.31  639        XP_019795730.1   
hypothetical protein EI555_002147 [Monodon monoceros]             Monodon mono... narwhal         40151      42.0   42.0  12%   0.030 31.31  639        TKC44060.1       
kelch-like protein 36 isoform X1 [Physeter catodon]               Physeter cat... sperm whale     9755       42.0   42.0  12%   0.030 31.31  639        XP_007109283.1   
hypothetical protein DBR06_SOUSAS7010052 [Sousa chinensis]        Sousa chinensis Indo-pacific... 103600     42.0   42.0  12%   0.030 31.31  588        TEA30942.1       
kelch-like protein 36 isoform X1 [Balaenoptera acutorostrata...   Balaenoptera... NA              310752     42.0   42.0  12%   0.030 31.31  616        XP_007172654.1   
kelch-like protein 36 isoform X2 [Balaenoptera musculus]          Balaenoptera... Blue whale      9771       42.0   42.0  12%   0.031 31.31  622        XP_036689788.1   
kelch-like protein 36 isoform X2 [Phocoena sinus]                 Phocoena sinus  vaquita         42100      42.0   42.0  12%   0.031 31.31  616        XP_032469050.1   
kelch-like protein 36 isoform X1 [Lagenorhynchus obliquidens]     Lagenorhynch... Pacific whit... 90247      42.0   42.0  12%   0.031 31.31  639        XP_026974899.1   
kelch-like protein 36 [Neophocaena asiaeorientalis...             Neophocaena ... Yangtze finl... 1706337    42.0   42.0  12%   0.032 31.31  616        XP_024591297.1   
PREDICTED: kelch-like protein 36 isoform X2 [Lipotes vexillifer]  Lipotes vexi... Yangtze Rive... 118797     42.0   42.0  12%   0.032 31.31  616        XP_007468472.1   
kelch-like protein 36 isoform X2 [Delphinapterus leucas]          Delphinapter... beluga whale    9749       42.0   42.0  12%   0.033 31.31  616        XP_022438731.1   
kelch-like protein 36 isoform X2 [Tursiops truncatus]             Tursiops tru... common bottl... 9739       42.0   42.0  12%   0.033 31.31  616        XP_019795739.1   
kelch-like protein 36 isoform X2 [Orcinus orca]                   Orcinus orca    killer whale    9733       41.6   41.6  12%   0.034 31.31  616        XP_004280133.1   
kelch-like protein 36 isoform X3 [Balaenoptera musculus]          Balaenoptera... Blue whale      9771       41.6   41.6  12%   0.034 31.31  616        XP_036689790.1   
kelch-like protein 36 isoform X2 [Physeter catodon]               Physeter cat... sperm whale     9755       41.6   41.6  12%   0.034 31.31  616        XP_023980819.1   
kelch-like protein 36 isoform X2 [Lagenorhynchus obliquidens]     Lagenorhynch... Pacific whit... 90247      41.6   41.6  12%   0.034 31.31  616        XP_026974902.1   
kelch-like protein 36 isoform X2 [Balaenoptera acutorostrata...   Balaenoptera... NA              310752     41.6   41.6  12%   0.036 31.31  543        XP_007172655.1   
kelch-like protein 36 isoform X4 [Balaenoptera musculus]          Balaenoptera... Blue whale      9771       41.6   41.6  12%   0.037 31.31  543        XP_036689791.1   
kelch-like protein 36 isoform X3 [Tursiops truncatus]             Tursiops tru... common bottl... 9739       41.6   41.6  12%   0.038 31.31  543        XP_019795748.1   
kelch-like protein 36 isoform X3 [Lagenorhynchus obliquidens]     Lagenorhynch... Pacific whit... 90247      41.6   41.6  12%   0.038 31.31  543        XP_026974903.1   
PREDICTED: actin-binding protein IPP [Lipotes vexillifer]         Lipotes vexi... Yangtze Rive... 118797     41.6   41.6  8%    0.038 34.43  584        XP_007450437.1   
kelch-like protein 36 isoform X5 [Physeter catodon]               Physeter cat... sperm whale     9755       41.6   41.6  12%   0.039 31.31  543        XP_028333637.1   
actin-binding protein IPP isoform X3 [Physeter catodon]           Physeter cat... sperm whale     9755       41.2   41.2  8%    0.043 34.43  528        XP_028345226.1   

Alignments:

>PREDICTED: kelch-like protein 7 isoform X1 [Lipotes vexillifer]
Sequence ID: XP_007447506.1 Length: 586
Range 1: 380 to 487

Score:45.8 bits(107), Expect:0.002, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  380  AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRCSHGMVE-ANGLIYVCGGS  438

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  439  LGNNVSGRVL-NSCEVYDPATETWTELC-----PMIEARK-------NHGLVF--VKDKI  483

Query  387  FQAG  390
            F  G
Sbjct  484  FAVG  487




>kelch-like protein 7 isoform X1 [Orcinus orca]
Sequence ID: XP_004263486.1 Length: 586
>kelch-like protein 7 isoform X1 [Physeter catodon]
Sequence ID: XP_007116400.1 Length: 586
>kelch-like protein 7 isoform X1 [Tursiops truncatus]
Sequence ID: XP_019784188.1 Length: 586
>kelch-like protein 7 isoform X1 [Delphinapterus leucas]
Sequence ID: XP_022443730.1 Length: 586
>kelch-like protein 7 isoform X1 [Neophocaena asiaeorientalis asiaeorientalis]
Sequence ID: XP_024594248.1 Length: 586
>kelch-like protein 7 isoform X1 [Lagenorhynchus obliquidens]
Sequence ID: XP_026958470.1 Length: 586
>kelch-like protein 7 isoform X1 [Monodon monoceros]
Sequence ID: XP_029089713.1 Length: 586
>kelch-like protein 7 isoform X1 [Globicephala melas]
Sequence ID: XP_030712916.1 Length: 586
>kelch-like protein 7 isoform X1 [Phocoena sinus]
Sequence ID: XP_032499338.1 Length: 586
>kelch-like protein 7 isoform X1 [Balaenoptera musculus]
Sequence ID: XP_036719344.1 Length: 586
>hypothetical protein DBR06_SOUSAS15810013 [Sousa chinensis]
Sequence ID: TEA12625.1 Length: 586 
Range 1: 380 to 487

Score:45.8 bits(107), Expect:0.002, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  380  AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRCSHGMVE-ANGLIYVCGGS  438

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  439  LGNNVSGRVL-NSCEVYDPATETWTELC-----PMIEARK-------NHGLVF--VKDKI  483

Query  387  FQAG  390
            F  G
Sbjct  484  FAVG  487




>kelch-like protein 7 isoform X2 [Orcinus orca]
Sequence ID: XP_004263487.1 Length: 538
>kelch-like protein 7 isoform X2 [Physeter catodon]
Sequence ID: XP_007116401.1 Length: 538
>kelch-like protein 7 isoform X2 [Physeter catodon]
Sequence ID: XP_007116402.1 Length: 538
>kelch-like protein 7 isoform X2 [Physeter catodon]
Sequence ID: XP_007116403.1 Length: 538
>PREDICTED: kelch-like protein 7 isoform X2 [Lipotes vexillifer]
Sequence ID: XP_007447507.1 Length: 538
>PREDICTED: kelch-like protein 7 isoform X3 [Lipotes vexillifer]
Sequence ID: XP_007447508.1 Length: 538
>kelch-like protein 7 isoform X2 [Tursiops truncatus]
Sequence ID: XP_019784193.1 Length: 538
>kelch-like protein 7 isoform X2 [Tursiops truncatus]
Sequence ID: XP_019784202.1 Length: 538
>kelch-like protein 7 isoform X2 [Tursiops truncatus]
Sequence ID: XP_019784211.1 Length: 538
>kelch-like protein 7 isoform X2 [Delphinapterus leucas]
Sequence ID: XP_022443739.1 Length: 538
>kelch-like protein 7 isoform X2 [Delphinapterus leucas]
Sequence ID: XP_022443748.1 Length: 538
>kelch-like protein 7 isoform X2 [Delphinapterus leucas]
Sequence ID: XP_022443758.1 Length: 538
>kelch-like protein 7 isoform X2 [Neophocaena asiaeorientalis asiaeorientalis]
Sequence ID: XP_024594249.1 Length: 538
>kelch-like protein 7 isoform X2 [Lagenorhynchus obliquidens]
Sequence ID: XP_026958471.1 Length: 538
>kelch-like protein 7 isoform X2 [Lagenorhynchus obliquidens]
Sequence ID: XP_026958472.1 Length: 538
>kelch-like protein 7 isoform X2 [Lagenorhynchus obliquidens]
Sequence ID: XP_026958473.1 Length: 538
>kelch-like protein 7 isoform X2 [Monodon monoceros]
Sequence ID: XP_029089714.1 Length: 538
>kelch-like protein 7 isoform X2 [Globicephala melas]
Sequence ID: XP_030712917.1 Length: 538
>kelch-like protein 7 isoform X2 [Globicephala melas]
Sequence ID: XP_030712918.1 Length: 538
>kelch-like protein 7 isoform X2 [Globicephala melas]
Sequence ID: XP_030712919.1 Length: 538
>kelch-like protein 7 isoform X2 [Phocoena sinus]
Sequence ID: XP_032499339.1 Length: 538
>kelch-like protein 7 isoform X2 [Phocoena sinus]
Sequence ID: XP_032499340.1 Length: 538
>kelch-like protein 7 isoform X2 [Orcinus orca]
Sequence ID: XP_033296255.1 Length: 538
>kelch-like protein 7 isoform X2 [Orcinus orca]
Sequence ID: XP_033296259.1 Length: 538
>kelch-like protein 7 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036719345.1 Length: 538
>kelch-like protein 7 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036719346.1 Length: 538
>kelch-like protein 7 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036719347.1 Length: 538
>kelch-like protein 7 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036719348.1 Length: 538
>kelch-like protein 7 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036719349.1 Length: 538 
Range 1: 332 to 439

Score:45.8 bits(107), Expect:0.002, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  332  AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRCSHGMVE-ANGLIYVCGGS  390

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  391  LGNNVSGRVL-NSCEVYDPATETWTEL-----CPMIEARK-------NHGLVF--VKDKI  435

Query  387  FQAG  390
            F  G
Sbjct  436  FAVG  439




>PREDICTED: kelch-like protein 7 isoform X4 [Lipotes vexillifer]
Sequence ID: XP_007447509.1 Length: 510
>kelch-like protein 7 isoform X3 [Monodon monoceros]
Sequence ID: XP_029089715.1 Length: 510 
Range 1: 304 to 411

Score:45.4 bits(106), Expect:0.002, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  304  AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRCSHGMVE-ANGLIYVCGGS  362

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  363  LGNNVSGRVL-NSCEVYDPATETWTEL-----CPMIEARK-------NHGLVF--VKDKI  407

Query  387  FQAG  390
            F  G
Sbjct  408  FAVG  411




>kelch-like protein 7 [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_007170379.1 Length: 361
Range 1: 155 to 262

Score:45.1 bits(105), Expect:0.003, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  155  AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRCSHGMVE-ANGLIYVCGGS  213

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  214  LGNNVSGRVL-NSCEVYDPATETWTEL-----CPMIEARK-------NHGLVF--VKDKI  258

Query  387  FQAG  390
            F  G
Sbjct  259  FAVG  262




>hypothetical protein E2I00_016144, partial [Balaenoptera physalus]
Sequence ID: KAB0388793.1 Length: 287
Range 1: 81 to 188

Score:44.3 bits(103), Expect:0.003, 
Method:Compositional matrix adjust., 
Identities:37/124(30%), Positives:57/124(45%), Gaps:25/124(20%)

Query  276  GNGQIVVTGGNDAKKTSLY-----DSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS  330
              G+I  +GG++   ++LY     D+ ++SW   P M   R        ++G ++  GGS
Sbjct  81   AEGKIYTSGGSEVGNSALYLFECYDTRTESWHTKPSMLTQRC-SHGMVEANGLIYVCGGS  139

Query  331  W----SGGVFEKNGEVYSPSSKTWTSLPNAKVNPMLTADKQGLYRSDNHAWLFGWKKGSV  386
                 SG V   + EVY P+++TWT L      PM+ A K       NH  +F   K  +
Sbjct  140  LGNNVSGRVL-NSCEVYDPATETWTEL-----CPMIEARK-------NHGLVF--VKDKI  184

Query  387  FQAG  390
            F  G
Sbjct  185  FAVG  188




>hypothetical protein E2I00_018042 [Balaenoptera physalus]
Sequence ID: KAB0395624.1 Length: 630
Range 1: 367 to 452

Score:42.4 bits(98), Expect:0.024, 
Method:Compositional matrix adjust., 
Identities:25/88(28%), Positives:43/88(48%), Gaps:7/88(7%)

Query  277  NGQIVVTGGNDAKK----TSLYDSSSDSWIPGPDMQVARG-YQSSATMSDGRVFTIGGSW  331
            NG+++  GG + ++       YD  +D W     M+  R  +Q +  M  G+++ +GGS 
Sbjct  367  NGKLIAAGGYNREECLRTVECYDPHTDHWSFLAPMRTPRARFQMAVLM--GQLYVVGGSN  424

Query  332  SGGVFEKNGEVYSPSSKTWTSLPNAKVN  359
                  + GEVY P+   WT +P  + N
Sbjct  425  GHSDDLRCGEVYDPNIDDWTPIPELRTN  452




>PREDICTED: kelch-like protein 36 isoform X3 [Lipotes vexillifer]
Sequence ID: XP_007468473.1 Length: 553
Range 1: 383 to 475

Score:42.0 bits(97), Expect:0.025, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  383  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  441

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  442  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  475




>kelch-like protein 36 isoform X3 [Physeter catodon]
Sequence ID: XP_028333635.1 Length: 576
Range 1: 406 to 498

Score:42.0 bits(97), Expect:0.026, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  406  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  464

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  465  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  498




>influenza virus NS1A-binding protein [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_007167032.1 Length: 642
>influenza virus NS1A-binding protein [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_028017891.1 Length: 642
>influenza virus NS1A-binding protein [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_028017892.1 Length: 642
>influenza virus NS1A-binding protein [Balaenoptera musculus]
Sequence ID: XP_036699678.1 Length: 642
>influenza virus NS1A-binding protein [Balaenoptera musculus]
Sequence ID: XP_036699687.1 Length: 642
>influenza virus NS1A-binding protein [Balaenoptera musculus]
Sequence ID: XP_036699697.1 Length: 642 
Range 1: 367 to 452

Score:42.0 bits(97), Expect:0.026, 
Method:Compositional matrix adjust., 
Identities:25/88(28%), Positives:43/88(48%), Gaps:7/88(7%)

Query  277  NGQIVVTGGNDAKKT----SLYDSSSDSWIPGPDMQVARG-YQSSATMSDGRVFTIGGSW  331
            NG+++  GG + ++       YD  +D W     M+  R  +Q +  M  G+++ +GGS 
Sbjct  367  NGKLIAAGGYNREECLRTVECYDPHTDHWSFLAPMRTPRARFQMAVLM--GQLYVVGGSN  424

Query  332  SGGVFEKNGEVYSPSSKTWTSLPNAKVN  359
                  + GEVY P+   WT +P  + N
Sbjct  425  GHSDDLRCGEVYDPNIDDWTPIPELRTN  452




>PREDICTED: kelch-like protein 36 isoform X1 [Lipotes vexillifer]
Sequence ID: XP_007468471.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.027, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>hypothetical protein E2I00_019412 [Balaenoptera physalus]
Sequence ID: KAB0392230.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.028, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Delphinapterus leucas]
Sequence ID: XP_022438730.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.028, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Phocoena sinus]
Sequence ID: XP_032469049.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.028, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Balaenoptera musculus]
Sequence ID: XP_036689786.1 Length: 639
>kelch-like protein 36 isoform X1 [Balaenoptera musculus]
Sequence ID: XP_036689787.1 Length: 639 
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.029, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Globicephala melas]
Sequence ID: XP_030719029.1 Length: 639
>kelch-like protein 36 isoform X1 [Globicephala melas]
Sequence ID: XP_030719030.1 Length: 639
>kelch-like protein 36 isoform X1 [Orcinus orca]
Sequence ID: XP_033262243.1 Length: 639
>kelch-like protein 36 isoform X1 [Orcinus orca]
Sequence ID: XP_033262244.1 Length: 639 
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.029, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Tursiops truncatus]
Sequence ID: XP_019795730.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.030, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>hypothetical protein EI555_002147 [Monodon monoceros]
Sequence ID: TKC44060.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.030, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 isoform X1 [Physeter catodon]
Sequence ID: XP_007109283.1 Length: 639
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.030, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>hypothetical protein DBR06_SOUSAS7010052, partial [Sousa chinensis]
Sequence ID: TEA30942.1 Length: 588
Range 1: 419 to 511

Score:42.0 bits(97), Expect:0.030, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  419  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  477

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  478  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  511




>kelch-like protein 36 isoform X1 [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_007172654.1 Length: 616
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.030, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036689788.1 Length: 622
>kelch-like protein 36 isoform X2 [Balaenoptera musculus]
Sequence ID: XP_036689789.1 Length: 622 
Range 1: 452 to 544

Score:42.0 bits(97), Expect:0.031, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  452  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  510

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  511  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  544




>kelch-like protein 36 isoform X2 [Phocoena sinus]
Sequence ID: XP_032469050.1 Length: 616
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.031, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X1 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974899.1 Length: 639
>kelch-like protein 36 isoform X1 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974900.1 Length: 639 
Range 1: 469 to 561

Score:42.0 bits(97), Expect:0.031, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  469  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  527

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  528  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  561




>kelch-like protein 36 [Neophocaena asiaeorientalis asiaeorientalis]
Sequence ID: XP_024591297.1 Length: 616
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.032, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>PREDICTED: kelch-like protein 36 isoform X2 [Lipotes vexillifer]
Sequence ID: XP_007468472.1 Length: 616
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.032, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Delphinapterus leucas]
Sequence ID: XP_022438731.1 Length: 616
>kelch-like protein 36 [Monodon monoceros]
Sequence ID: XP_029094326.1 Length: 616 
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.033, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Tursiops truncatus]
Sequence ID: XP_019795739.1 Length: 616
Range 1: 446 to 538

Score:42.0 bits(97), Expect:0.033, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Orcinus orca]
Sequence ID: XP_004280133.1 Length: 616
>kelch-like protein 36 isoform X2 [Globicephala melas]
Sequence ID: XP_030719031.1 Length: 616 
Range 1: 446 to 538

Score:41.6 bits(96), Expect:0.034, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X3 [Balaenoptera musculus]
Sequence ID: XP_036689790.1 Length: 616
>kelch-like protein 36 isoform X3 [Balaenoptera musculus]
Sequence ID: XP_036689794.1 Length: 616 
Range 1: 446 to 538

Score:41.6 bits(96), Expect:0.034, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Physeter catodon]
Sequence ID: XP_023980819.1 Length: 616
Range 1: 446 to 538

Score:41.6 bits(96), Expect:0.034, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974902.1 Length: 616
Range 1: 446 to 538

Score:41.6 bits(96), Expect:0.034, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  446  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  504

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  505  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  538




>kelch-like protein 36 isoform X2 [Balaenoptera acutorostrata scammoni]
Sequence ID: XP_007172655.1 Length: 543
Range 1: 373 to 465

Score:41.6 bits(96), Expect:0.036, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  373  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  431

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  432  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  465




>kelch-like protein 36 isoform X4 [Balaenoptera musculus]
Sequence ID: XP_036689791.1 Length: 543
>kelch-like protein 36 isoform X4 [Balaenoptera musculus]
Sequence ID: XP_036689792.1 Length: 543 
Range 1: 373 to 465

Score:41.6 bits(96), Expect:0.037, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  373  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  431

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  432  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  465




>kelch-like protein 36 isoform X3 [Tursiops truncatus]
Sequence ID: XP_019795748.1 Length: 543
>kelch-like protein 36 isoform X3 [Tursiops truncatus]
Sequence ID: XP_019795756.1 Length: 543
>kelch-like protein 36 isoform X3 [Tursiops truncatus]
Sequence ID: XP_019795765.1 Length: 543
>kelch-like protein 36 isoform X3 [Globicephala melas]
Sequence ID: XP_030719033.1 Length: 543
>kelch-like protein 36 isoform X3 [Globicephala melas]
Sequence ID: XP_030719034.1 Length: 543
>kelch-like protein 36 isoform X3 [Globicephala melas]
Sequence ID: XP_030719035.1 Length: 543
>kelch-like protein 36 isoform X3 [Orcinus orca]
Sequence ID: XP_033262245.1 Length: 543
>kelch-like protein 36 isoform X3 [Orcinus orca]
Sequence ID: XP_033262246.1 Length: 543
>kelch-like protein 36 isoform X3 [Orcinus orca]
Sequence ID: XP_033262247.1 Length: 543 
Range 1: 373 to 465

Score:41.6 bits(96), Expect:0.038, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  373  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  431

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  432  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  465




>kelch-like protein 36 isoform X3 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974903.1 Length: 543
>kelch-like protein 36 isoform X3 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974904.1 Length: 543
>kelch-like protein 36 isoform X3 [Lagenorhynchus obliquidens]
Sequence ID: XP_026974905.1 Length: 543 
Range 1: 373 to 465

Score:41.6 bits(96), Expect:0.038, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  373  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  431

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  432  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  465




>PREDICTED: actin-binding protein IPP [Lipotes vexillifer]
Sequence ID: XP_007450437.1 Length: 584
Range 1: 410 to 469

Score:41.6 bits(96), Expect:0.038, 
Method:Compositional matrix adjust., 
Identities:21/61(34%), Positives:33/61(54%), Gaps:1/61(1%)

Query  294  YDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGSWSGGVFEKNGEVYSPSSKTWTSL  353
            +DS  + W    +M ++R Y     M  G ++ IGG  + G+  ++ EVY P SK W+ L
Sbjct  410  FDSDENKWEVVGNMAMSRYYFGCCEMQ-GLIYVIGGISNEGIELRSFEVYDPLSKRWSPL  468

Query  354  P  354
            P
Sbjct  469  P  469




>kelch-like protein 36 isoform X5 [Physeter catodon]
Sequence ID: XP_028333637.1 Length: 543
Range 1: 373 to 465

Score:41.6 bits(96), Expect:0.039, 
Method:Compositional matrix adjust., 
Identities:31/99(31%), Positives:47/99(47%), Gaps:17/99(17%)

Query  280  IVVTGGND------AKKTSLYDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGS---  330
            + ++GG+D       K    YD  +D W     M  ARG+ S  ++ D  +++IGGS   
Sbjct  373  VYISGGHDYQIGPYRKNLLCYDHRTDVWEERRPMTTARGWHSMCSLGDS-IYSIGGSDDS  431

Query  331  -WSGGVFEKNG-EVYSPSSKTWTSLPNAKVNPMLTADKQ  367
              S   F+  G E YSP    WT     +V P+L A+ +
Sbjct  432  LESMERFDVLGVEAYSPQCNQWT-----RVAPLLHANSE  465




>actin-binding protein IPP isoform X3 [Physeter catodon]
Sequence ID: XP_028345226.1 Length: 528
Range 1: 354 to 413

Score:41.2 bits(95), Expect:0.043, 
Method:Compositional matrix adjust., 
Identities:21/61(34%), Positives:32/61(52%), Gaps:1/61(1%)

Query  294  YDSSSDSWIPGPDMQVARGYQSSATMSDGRVFTIGGSWSGGVFEKNGEVYSPSSKTWTSL  353
            +D   + W    +M +AR Y     M  G ++ IGG  + G+  ++ EVY P SK W+ L
Sbjct  354  FDPDENKWEVVGNMAMARYYFGCCEMQ-GLIYVIGGISNEGIELRSFEVYDPLSKRWSPL  412

Query  354  P  354
            P
Sbjct  413  P  413




		


