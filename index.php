<?php

$combo = isset($_REQUEST['combo']) ?? false;
$similarities = isset($_REQUEST['similarities']) ?? false;
$rid = "";

if ($combo || $similarities) {
	$rid = trim($_REQUEST['rid']) ?? "";
	if (empty($rid)) {
		echo "Please enter a valid RID";
		exit(1);
	}
}

if ($combo) {
	$opt = array(
	  'http' => array(
		'method' => 'GET',
		'ignore_errors' => true
	  )
	);
	$content = stream_context_create($opt);
	$hittablecontent = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=CSV&DESCRIPTIONS=100&FORMAT_OBJECT=Alignment&ALIGNMENT_VIEW=Tabular&CMD=Get", false, $content);
	$descriptionstable = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=CSV&DESCRIPTIONS=100&FORMAT_OBJECT=Alignment&QUERY_INDEX=0&DOWNLOAD_TEMPL=Results&CMD=Get", false, $content);
	
	if (!$hittablecontent || !$descriptionstable) {
		echo "error occurred";
		exit();
	}
	
	$fname = "output.csv";
	
	// mutate data to output
	$lines = explode(PHP_EOL, $hittablecontent);
	$array = array();
	foreach ($lines as $line) {
		$array[] = str_getcsv($line);
	}
	$otherlines = explode(PHP_EOL, $descriptionstable);
	$otherarray = array();
	foreach ($otherlines as $otherline) {
		$otherarray[] = str_getcsv($otherline);
	}
	
	// construct dictionary of Accession -> Scientific Name
	$namelookups = array();
	foreach ($otherarray as $desc) {
		$name = $desc[1];
		$accession = explode(",", $desc[10])[1];
		$accession = substr($accession, 1, strlen($accession)-3);
		$namelookups[$accession] = $name;
	}
	
	foreach($array as &$piece) {
		$acc = $piece[1];
		$piece[] = $namelookups[$acc];
	}
	
	// convert back to a string to output as CSV
	$newtext = "";
	foreach ($array as $part) {
		if ($part[0]) {
			$partstr = implode(',', $part);
			$partstr .= "\n";
			$newtext .= $partstr;
		}
	}
	
	// Add headers
	$headertext = ['query acc.ver', 'subject acc.ver', '% identity', 'alignment length', 'mismatches', 'gap opens', 'q. start', 'q. end', 's. start', 's. end', 'evalue', 'bit score', '% positives', 'Scientific Name'];
	$headertext = implode(",", $headertext);
	$newtext = "$headertext\n$newtext";
	
	header( 'Content-Type: application/csv' );
    header( 'Content-Disposition: attachment; filename="' . $fname . '";' );

    // clean output buffer
    ob_end_clean();
    
    $handle = fopen( 'php://output', 'w' );

    fwrite($handle, $newtext);

    fclose( $handle );

    // flush buffer
    ob_flush();
    
    // use exit to get rid of unexpected output afterward
    exit();
}

if ($similarities) {
	$opt = array(
	  'http' => array(
		'method' => 'GET'
	  )
	);
	$content = stream_context_create($opt);
	$output = file_get_contents("https://blast.ncbi.nlm.nih.gov/blast/Blast.cgi?RESULTS_FILE=on&RID=$rid&FORMAT_TYPE=Text&DESCRIPTIONS=100&ALIGNMENTS=100&FORMAT_OBJECT=Alignment&QUERY_INDEX=0&DOWNLOAD_TEMPL=Results&CMD=Get");
}

?>

<!doctype html>
<html>
	<head>
		<title>BIOL 317 Scripts S2022</title>
		<script type="text/javascript" src="script.js"></script>
		<script>
			var myContent = "";
			onload = function () {
				if (document.getElementById('similarities')) {
					myContent = document.getElementById('output').innerHTML.trim();
					Array.prototype.slice.call(document.getElementsByTagName('pre')).forEach((l) => {
						l.parentNode.removeChild(l);
					});
					let outputElem = document.createElement('pre');
					outputElem.setAttribute("id", "output");
					document.body.appendChild(outputElem);
					dealWithContent(myContent);
				}
			document.getElementById('goMulti').addEventListener("click", function (e) {
				e.preventDefault();
				var reader = new FileReader();
				reader.addEventListener('load', function() {
					content = reader.result;
					console.log(content.length);
					dealWithContent(content.trim());
				})
				reader.readAsText(document.getElementById('given').files[0]);

				// reset DOM
				Array.prototype.slice.call(document.getElementsByTagName('pre')).forEach((l) => {
					l.parentNode.removeChild(l);
				});
				let outputElem = document.createElement('pre');
				outputElem.setAttribute("id", "output");
				document.body.appendChild(outputElem);
			});
			}
		</script>
		<style type="text/css">
			.sample-print {
				display: flex;
				justify-content: space-between;
			}
		</style>
	</head>
	<body id="<?php echo $similarities ? 'similarities' : ''; ?>">
		<?php if ($similarities) : ?>
			<?php echo "<pre id='output'>$output</pre>"; ?>
		<?php else: ?>
		<h1>BIOL 317 Spring 2022</h1>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<label>RID:</label>
			<input type="text" name="rid">
			<br>
			<br>
			<input type="submit" name="similarities" value="Similarities"/>
			<input type="submit" name="combo" value="Combine CSV Output"/>
		</form>
		
		<h2>Upload files for similarities</h2>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<label>Files:</label>
			<input type="file" id="given">
			<button id="goMulti">Submit</button>
		</form>
		<?php endif; ?>
	</body>
</html>