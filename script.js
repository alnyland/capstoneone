const outputLineTitle = "Analysis";
const identChar = '=';
const posChar   = '^';
const gapChar   = '#';
var title;
var content;
var lines;

var matchLines = [];

// onload
function dealWithContent(content) {
	print(content);
	//var lines = content.split('\n');
	lines = content.split('\n');
	if (isSingle(lines[0].split(' ')[0])) {
		print(dealWithSingleFile(lines));
	} else {
		dealWithLongFile(lines);
	}
	// Chart printing
	var l = document.getElementsByClassName('sample-print'), j=0;
	for (var i=0; i<l.length; i++) {
		if (!(l[i].getElementsByTagName('span').length > 0)) {continue;}
		var elem = document.createElement('div');
		elem.innerHTML = l[i].innerHTML;
		l[i].innerHTML = "";
		l[i].append(elem);
		var chartElem = document.createElement('div');
		if (matchLines[j] && Array.isArray(matchLines[j])) {
			chartElem.innerHTML = matchLines[j++].join("\n") + "\n";
			l[i].append(chartElem);
			var downloadLink = document.createElement('a');
			
		  	downloadLink.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(chartElem.innerHTML));
		  	downloadLink.setAttribute('download', "summary" + (j-1) + ".txt");
			downloadLink.innerHTML = "Download";

			chartElem.append(downloadLink);
		}
	}
}

function dealWithSingleFile(lines) {
	let startLine = lines.findIndex((l) => {return (l.charAt(0) == '>' || l.startsWith("&gt"));});
	title = lines[startLine].substr(1);
	const counter = new FileCounts(title);
	var matchLinesI = 0;
	for (var i=startLine + 8; i<lines.length; i+=5) {
		let query = lines[i];
		let matching = lines[i+1];
		let subject = lines[i+2];
		

		// count gaps + positives + identities - after methodology is determined
		analyizeLine(query, matching, subject, counter);

		if (counter.lineStartPos && matching) {
			var addLine = counter.print();
			if (!Array.isArray(matchLines[matchLinesI])) {
				matchLines[matchLinesI] = [];
			}
			matchLines[matchLinesI++].push(
				addLine.slice(outputLineTitle.length + 4)
					.replace('/', ' ')
					.replace('/', ' ')
			);
			
			lines.splice(i+3, 0, addLine);
		}
		counter.newLine();
	}
	return lines.join('\n');
}

function dealWithLongFile(lines) {
	var filesOutput = [];
	// find top of list of alignments
	let chartStartIndex = lines.findIndex((l) => {return l.search("Description") >= 0});
	let chartStart = lines[chartStartIndex + 1];

	// find length of full name (for link content)
	let nameLength = lines[chartStartIndex].search("Name");

	// find alignment section starts
	let sectionStarts = [];
	lines.map((l, i, a) => {
		if (l.charAt(0) == '>' || l.startsWith("&gt")) {
			sectionStarts.push(i);
		}
	});
	
	// grab unique tags for the anchor headings for each section
	let taxStart = lines[chartStartIndex].search("Accession");
	var taxIDs = [];
	

	// create links in chart of alignments
	for (var i=chartStartIndex + 1; !lines[i].includes("Alignments") && lines[i] != ''; i++) {
		let taxEnd = lines[i].indexOf(' ', taxStart + 1);
		let taxID = lines[i].substr(taxStart, taxEnd - taxStart);
		taxIDs.push(taxID);
		// convert text to links
		lines[i] = '<a href="#' + taxID + '">' 
			+ lines[i].substr(0, nameLength) 
			+ '</a>' 
			+ lines[i].substr(nameLength);
	}

	var newSectionStarts = [];
	for (var i=1; i<sectionStarts.length - 1; i++) {
		if (sectionStarts[i] - sectionStarts[i-1] > 2) {
			newSectionStarts.push(sectionStarts[i-1]);
		}
	}
	
	
	// give alignment sections to dealWithSingleFile()
	var j = 0;
	for (var i=0; i < sectionStarts.length; i++) {
		let start = sectionStarts[i];
		let end = (i == sectionStarts.length-1) ? lines.length + 1 : sectionStarts[i + 1];
		// can't be lines[start] as that line is parsed later, can't be [start-1] as that isn't passed
		if (sectionStarts[i+1] - sectionStarts[i] > 2 || i == sectionStarts.length - 1) {
			lines[start+1] = '<span id="' + taxIDs[j++] + '">' + lines[start+1] + ".</span>";
		}
		filesOutput.push(dealWithSingleFile(lines.slice(start, end)));
	}

	// put into DOM
	filesOutput.forEach((l) => {
		let elem = document.createElement('pre');
		elem.classList.add('sample-print');
		elem.innerHTML = l;
		document.body.appendChild(elem);
	});
	print(lines.slice(0, sectionStarts[0]).join('\n'));
}

// Constructs
function analyizeLine(query, matching, subject, filecounter) {
	if (matching) {
		filecounter.setLineStart(
			Math.min(
				query.substr(5).search(/[A-Z]/g) + 5,
				subject.substr(5).search(/[A-Z]/g) + 5
			));
		filecounter.setLineLength(findLastPart(query));
		filecounter.foundIndentities((matching.match(/[A-Z]/g) || []).length);
		filecounter.foundPositives((matching.match(/\+/g) || []).length);
		filecounter.foundGaps((subject.match(/\-/g) || []).length);
		filecounter.considerLine(query, matching, subject); // maybe count inside this later as well
	}
}
class FileCounts {
	constructor(name) {
		this.name = name;
		this.identities = 0;
		this.positives = 0;
		this.gaps = 0;
		this.totalIdentities = 0;
		this.totalPositives = 0;
		this.totalGaps = 0;
		this.length = 0;
		this.lineStartPos = 0;
		this.outputLine = identChar.repeat(60);
	}
	newLine() {
		this.identities = 0;
		this.positives = 0;
		this.gaps = 0;
		this.length = 0;
	}
	considerLine(q, m, s) {
		let myLength = this.length - this.lineStartPos - 1;
		let query = q.substr(this.lineStartPos, this.length);
		let matching = m.substr(this.lineStartPos, this.length);
//					let matching = m.trim();
		let subject = s.substr(this.lineStartPos, this.length);
		this.outputLine = identChar.repeat(myLength);
		for (var i=0; i<myLength; i++) {
			// identity set in constructor
			// gaps
			if (query.charAt(i) == '-' || subject.charAt(i) == '-') {
				this.outputLine = setCharAt(this.outputLine, i, gapChar);
			}
			// positives
			if (matching.charAt(i).match(/\+/g)) {
				this.outputLine = setCharAt(this.outputLine, i, posChar);
			}
		}
	}
	print() {
		let dataOutput = outputLineTitle + ' '.repeat(this.lineStartPos-8) + this.outputLine + " ".repeat();
		return dataOutput + ' '.repeat(2) + this.identities + '/' + (this.identities + this.positives) + '/' + this.gaps;
	}
	foundIdentity() {
		this.identities++;
	}
	foundIndentities(num) {
		this.identities = num;
		this.totalIdentities += num;
	}
	foundPositive() {
		this.positives++;
	}
	foundPositives(num) {
		this.positives = num;
		this.totalPositives += num;
	}
	foundGap() {
		this.gaps++;
	}
	foundGaps(num) {
		this.gaps = num;
		this.totalGaps += num;
	}
	setLineLength(size) {
		this.length = size;
	}
	getLineLength() {
		return this.length;
	}
	setLineStart(num) {
		this.lineStartPos = num;
	}
}

function setCharAt(str,index,chr) {
	if(index > str.length-1) return str;
	return str.substring(0,index) + chr + str.substring(index+1);
}

function findLastPart(str) {
	for (var i=str.length; i>0; i--) {
		if (str.charAt(i).match(/\D/i)) {
			return i;
		}
	}
	return -1;
}

function isSingle(txt) {
	return txt == "Query:"
}
function print(content) {
	document.getElementById('output').innerHTML = content;
}